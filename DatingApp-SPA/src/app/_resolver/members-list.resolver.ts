import { Injectable } from "@angular/core";
import { User } from '../_models/user';
import {Resolve, Router, ActivatedRouteSnapshot} from '@angular/router';
import { UserService } from '../_service/user.service';
import { AlertifyService } from '../_service/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()

export class MembersListResolver implements Resolve<User[]> {
     pageSize  = 5;
     pageNumber = 1;
     constructor(private userService: UserService, private router: Router,
                 private alertify: AlertifyService) {}

                 resolve(): Observable<User[]> {
                    return this.userService.getUsers(this.pageNumber, this.pageSize).pipe(
                         catchError (error => {
                                this.alertify.error(error);
                                this.router.navigate(['/home']);
                                return of(null);
                         })
                    );
                 }
}