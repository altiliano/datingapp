import { Photo } from './../../_models/photo';
import { AdminService } from './../../_service/admin.service';
import { AuthService } from './../../_service/auth.service';
import { AlertifyService } from './../../_service/alertify.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-photo-management',
  templateUrl: './photo-management.component.html',
  styleUrls: ['./photo-management.component.css']
})
export class PhotoManagementComponent implements OnInit {
  photos: Photo[];

  constructor(private alertify: AlertifyService, private authService: AuthService, private adminService: AdminService ) { }

  ngOnInit() {
    this.getPendingAprovalPhotos();
  }

  getPendingAprovalPhotos() {
    this.adminService.getPendingAprovalPhotos().subscribe((values: Photo[]) => {
       this.photos = values;
    }, error => {
       this.alertify.error(error);
    });
  }

  approvePhoto(photoId: number)  {
    this.adminService.approvePhoto(photoId).subscribe( () =>  {
      this.alertify.success('Photo approved succesfull!');
      this.getPendingAprovalPhotos();
    }, error => {
      console.log(error);
      this.alertify.error(error);
    });
  }

  rejectPhoto(photoId: number)   {
    this.adminService.rejectPhoto(photoId).subscribe(() => {
      this.alertify.success('Rejected photo succesfull');
      this.getPendingAprovalPhotos();
    }, error =>{
      this.alertify.error(error);
    });
  }

}
