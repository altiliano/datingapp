import { BsModalRef } from 'ngx-bootstrap';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/_models/user';

@Component({
  selector: 'app-role-model',
  templateUrl: './role-model.component.html',
  styleUrls: ['./role-model.component.css']
})
export class RoleModelComponent implements OnInit {
  @Output() updateSelectedRoles = new EventEmitter();
  user: User;
  closeBtnName: string;
  roles: any[];

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }

  updateRoles() {
    this.updateSelectedRoles.emit(this.roles);
    this.bsModalRef.hide();
  }

}
