import { AuthService } from './../_service/auth.service';
import { Directive, Input, ViewContainerRef, TemplateRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appHasRole]'
})
export class HasRoleDirective implements OnInit {
  @Input() appHasRole: string[];
  isVisible =  false;

  constructor(private viewContainerRef: ViewContainerRef, private tempplateRef: TemplateRef<any>, private authService: AuthService) { }

  ngOnInit() {
    const userRole = this.authService.decodedToken.role as Array<string>;

    if (!userRole) {
       this.viewContainerRef.clear();
    }

    if (this.authService.roleMatch(this.appHasRole)) {
     if  (!this.isVisible)  {
       this.isVisible = true;
       this.viewContainerRef.createEmbeddedView(this.tempplateRef);
     } else  {
       this.isVisible = false;
       this.viewContainerRef.clear();
     }
   }

  }

}
