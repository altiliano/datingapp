import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/_models/user';
import { UserService } from 'src/app/_service/user.service';
import { AlertifyService } from 'src/app/_service/alertify.service';
import { ActivatedRoute } from '@angular/router';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation,  NgxGalleryModule } from 'ngx-gallery';
import { TabsetComponent } from 'ngx-bootstrap';

@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.css']
})
export class MemberDetailComponent implements OnInit {
 user: User;
 galleryOptions: NgxGalleryOptions[];
 galleryImages: NgxGalleryImage[];
 @ViewChild('memberTabs', {static: true}) meberTabs: TabsetComponent;
  constructor(private userService: UserService, private alertify: AlertifyService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe( data => {
        this.user = data['user'];
    });

    this.route.queryParams.subscribe(params =>{
      const selectTab = params['tab'];
      this.meberTabs.tabs[selectTab > 0 ? selectTab :  0].active = true;
    })
    this.galleryOptions = [
       {
         width: '500px',
         height: '500px',
         imagePercent: 100,
         thumbnailsColumns: 4,
         imageAnimation: NgxGalleryAnimation.Slide,
         preview: false
       }
     ];
    this.galleryImages = this.getImages();
  }

  getImages() {
    const imageUrl = [];
    for ( const photo of this.user.photos)  {
           imageUrl.push({
              small: photo.url,
              medium: photo.url,
              big: photo.url,
              description: photo.description
           });
     }
    return imageUrl;
  }

 selectTab(tabId: number)  {
   this.meberTabs.tabs[tabId].active = true;
 }
}
