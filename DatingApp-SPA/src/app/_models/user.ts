import { Photo } from './photo';

export interface User {
    id: number;
    userName: string;
    age: number;
    gender: string;
    created: Date;
    lastActive: any;
    knownAs: string;
    city: string;
    country: string;
    photoUrl: string;
    interests?: string;
    introduction?: string;
    lookingFor?: string;
    photos?: Photo[];
    roles?: string[];
}
