import { HttpClient, HttpInterceptor } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { User } from '../_models/user';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  baseUrl = environment.apiUrl;

constructor(private http: HttpClient) { }

getUserWithRoles() {
    return this.http.get(this.baseUrl + 'admin/usersWithRoles');
}

updateUserRoles(user: User, roles: {}) {
  return this.http.post(this.baseUrl + 'admin/editRoles/' + user.userName, roles);
}

getPendingAprovalPhotos() {
  return this.http.get(this.baseUrl + 'admin/photosForModerations');
}

approvePhoto(photoId: number)  {
  return this.http.post(this.baseUrl + 'admin/approvePhoto/'  + photoId, {});
}

rejectPhoto(photoId: number)  {
  return this.http.delete(this.baseUrl + 'admin/rejectPhoto/'  + photoId );
}

}
