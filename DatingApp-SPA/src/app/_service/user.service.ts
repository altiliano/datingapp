import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from '../_models/user';
import { Observable } from 'rxjs';
import { PaginatedResolver } from '../_models/pagination';
import { map } from 'rxjs/operators';
import { Message } from '../_models/Message';

@Injectable({
  providedIn: 'root'
})
export class UserService {
baseUrl = environment.apiUrl;
constructor(private http: HttpClient) { }

getUsers(page?, itemsPerPage?, userParams?, likesParams?): Observable<PaginatedResolver<User[]>> {
  const paginateResult: PaginatedResolver<User[]> = new PaginatedResolver<User[]>();

  let params = new HttpParams();
  if (page != null && itemsPerPage != null) {
    params = params.append('pageNumber', page);
    params = params.append('pageSize', itemsPerPage);
  }

  if  (userParams != null)  {
    params = params.append('minAge', userParams.minAge);
    params = params.append('maxAge', userParams.maxAge);
    params = params.append('gender', userParams.gender);
    params = params.append('orderBy', userParams.orderBy);
  }

  if  ( likesParams === 'likers') {
     params = params.append('likers', 'true');
  }

  if  ( likesParams === 'likees') {
    params = params.append('likees', 'true');
 }
  return this.http.get<User[]>(this.baseUrl + 'users', {observe:  'response', params})
    .pipe(
        map(response => {
          paginateResult.result = response.body;
          if  (response.headers.get('Pagination') != null)  {
            paginateResult.pagination = JSON.parse(response.headers.get('Pagination'));
          }
          return paginateResult;
        })
    );
}

getUser(id): Observable<User> {
  return this.http.get<User>(this.baseUrl + 'users/' + id);
}

updateUser(id: number, user: User)  {
   return this.http.put(this.baseUrl + 'users/' + id, user);
}
setMainPhoto(userId: number, id: number)  {
    return this.http.post(this.baseUrl + 'users/' + userId + '/photos/' + id + '/setMain', {});
}

deletePhoto(userId: number, id: number) {
  return this.http.delete(this.baseUrl + 'users/' + userId + '/photos/' + id);
}

sendLike(id: number, recipient: number) {
   return this.http.post(this.baseUrl + 'users/' + id + '/like/' + recipient, {});
}

getMessages(id: number, page? , itemsPerPage?, messagecontainer?) {
  const pagenatedResult: PaginatedResolver<Message[]> = new PaginatedResolver<Message[]>();
  let params = new HttpParams();
  params = params.append('MessageContainer', messagecontainer);
  if (page != null && itemsPerPage != null) {
    params = params.append('pageNumber', page);
    params = params.append('pageSize', itemsPerPage);
  }

  return this.http.get<Message[]>(this.baseUrl + 'users/' + id + '/message', { observe: 'response', params})
    .pipe(
        map(response => {
          pagenatedResult.result = response.body;
          if  (response.headers.get('Pagination') !== null){
             pagenatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
           }
          return pagenatedResult;
        })
    );
}

getMessageThread(id: number, recipientId: number) {
  return this.http.get<Message[]>(this.baseUrl + 'users/' + id + '/message/thread/' + recipientId)
}

sendMessage(id: number, message: Message) {
  return this.http.post(this.baseUrl + 'users/' + id + '/message', message);
}

deleteMessage(id: number, userId: number) {
  return this.http.post(this.baseUrl + 'users/' + userId + '/message/' + id, {});
}
markAsRead(userId: number, messageId: number )  {
   this.http.post(this.baseUrl + 'users/' + userId + '/message/' + messageId + '/read', {})
    .subscribe();
}

}
