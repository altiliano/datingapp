using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatingApi.API.Helpers;
using DatingApi.API.Models;
using Microsoft.EntityFrameworkCore;

namespace DatingApi.API.Data
{
    public class DatingRepository : IDatingRepository
    {
        public readonly DataContext _context;
        public DatingRepository(DataContext context)
        {
            _context = context;

        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<User> GetUser(int id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
            return user;
        }

        public async Task<PagedList<User>> GetUsers(UserParams userParams)
        {
            var users =  _context.Users.OrderBy(u => u.LastActive).AsQueryable();
             users = users.Where(u => u.Id != userParams.UserId);
             users = users.Where(u => u.Gender == userParams.Gender);
            
       
            if (userParams.Likers)
            {
                var userLikers = await GetUsersLikes(userParams.UserId, userParams.Likers);
                users = users.Where( u => userLikers.Contains(u.Id));
            }

            if (userParams.Likees)
            {
                var userLikees = await GetUsersLikes(userParams.UserId, userParams.Likers);
                users = users.Where( u => userLikees.Contains(u.Id));   
            }
             
             if(userParams.MinAge != 18 || userParams.MaxAge != 99)
             {
                 var minDateOfBirth = DateTime.Today.AddYears(-userParams.MaxAge -1);
                 var maxDateOfBith = DateTime.Today.AddYears(-userParams.MinAge);
                 users = users.Where(u => u.DateOfBirth >= minDateOfBirth && u.DateOfBirth <= maxDateOfBith);
                
             }


             if(!string.IsNullOrEmpty(userParams.OrderBy))
             {
                    switch (userParams.OrderBy)
                    {
                        case "created":
                            users = users.OrderByDescending( u => u.Created);
                            break;
                        default:
                            users = users.OrderByDescending(u => u.LastActive);
                            break;

                    }
             }
            return await PagedList<User>.CreateAsync(users, userParams.PageNumber, userParams.PageSize);
        }

        public async Task<Photo> GetPhoto(int id)
        {
            var photo = await _context.Photos
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync(p => p.Id == id);
            return photo;
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
        public async Task<Photo> GetMainPhoto(int userId)
        {
            return await _context.Photos.IgnoreQueryFilters().Where(u => u.UserId == userId).FirstOrDefaultAsync(p => p.IsMainPhoto && p.IsAproved);
        }

        public async Task<Like> GetLike(int userId, int recipientId)
        {
            return await  _context.Likes.FirstOrDefaultAsync( u =>
             u.LikerId == userId && u.LikeeId == recipientId );
        }

        private async Task<IEnumerable<int>> GetUsersLikes ( int id, bool likers)
        {
             var users = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
            if(likers)
            {
                return users.Likers.Where( u => u.LikeeId == id).Select(i => i.LikerId);
            }
            else
            {
                return users.Likees.Where( u => u.LikerId == id).Select( i => i.LikeeId);
            }
        }

        public async Task<Message> GetMessage(int id)
        {
            return await _context.Messages.FirstOrDefaultAsync(m => m.Id == id);   
        }

        public async Task<PagedList<Message>> GetMessagesForUser(MessageParams messageParams)
        {
            var messages = _context.Messages.AsQueryable();
            switch (messageParams.MessageContainer){
                case "Inbox":
                    messages = messages.Where(u => u.RecipientId == messageParams.UserId && u.RecipientDeleted == false);
                    break;
                case "Outbox":
                    messages = messages.Where(u => u.SenderId == messageParams.UserId && u.SenderDeleted == false );
                    break;
                default:
                    messages = messages.Where(u => u.RecipientId == messageParams.UserId && u.IsRead == false && u.RecipientDeleted == false);
                    break;       
            }

            messages = messages.OrderByDescending(d => d.MessageSend);
            return await PagedList<Message>.CreateAsync(messages, messageParams.PageNumber, messageParams.PageSize);

        }

        public  async Task<IEnumerable<Message>> GetMessageThread(int userId, int recipientId)
        {
            var messages =  await _context.Messages.Where(m => m.RecipientId == userId && m.RecipientDeleted   ==  false && m.SenderId == recipientId 
                    ||  m.RecipientId == recipientId && m.SenderDeleted == false && m.SenderId == userId)
                .OrderByDescending( m =>m.MessageSend)
                .ToListAsync();
            return messages;            
        }

        public async Task<IEnumerable<Photo>> GetPhotosPendingAprove()
        {
            var photos = await _context.Photos.Include(u => u.User).IgnoreQueryFilters().Where(p => p.IsAproved == false).ToListAsync();
            return photos;
        }
    }
}