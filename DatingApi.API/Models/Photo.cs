using System;

namespace DatingApi.API.Models
{
    public class Photo
    {  
        public int Id { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public DateTime DateAdded { get; set; }
        public string PublicId { get; set; }
        public bool IsMainPhoto { get; set; }

        public virtual User User { get; set; }
        public int UserId { get; set; }
        public bool IsAproved { get; set; }
    }
}