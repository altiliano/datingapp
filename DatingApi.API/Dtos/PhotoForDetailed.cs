using System;

namespace DatingApi.API.Dtos
{
    public class PhotoForDetailed
    {
        
        public int Id { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public DateTime DateAdded { get; set; }
        public bool IsMainPhoto { get; set; }
        protected bool IsAproved {get; set;}
    }
}