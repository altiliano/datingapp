using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using DatingApi.API.Data;
using DatingApi.API.Dtos;
using DatingApi.API.Helpers;
using DatingApi.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace DatingApi.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AdminController :  ControllerBase
    {
        private readonly DataContext _context;
        private readonly UserManager<User> _userManager;

        private readonly IDatingRepository _repo;

        private readonly IMapper _mapper;

        private readonly IOptions<CloudinarySettings> _cloudinaryConfig;
        private readonly Cloudinary _cloudinary;

        public AdminController(DataContext context, UserManager<User> userManager, IDatingRepository repo, IMapper mapper, IOptions<CloudinarySettings> cloudinaryConfig)
        {
            _context = context;
            _userManager = userManager;
            _repo = repo;
            _mapper = mapper;
            _cloudinaryConfig = cloudinaryConfig;


            Account acc = new Account(
                _cloudinaryConfig.Value.CloudName,
                _cloudinaryConfig.Value.ApiKey,
                _cloudinaryConfig.Value.ApiSecret
            );
            _cloudinary = new Cloudinary(acc);

        }
        [Authorize(Policy = "RequiredAdminRole")]
        [HttpGet("usersWithRoles")]
        public async Task<IActionResult> GetUsersWithRoles()
        {
            var userList = await _context.Users
                .OrderBy(x => x.UserName)
                .Select(user => new
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    Roles = (from userRole in user.UserRoles
                                join role in _context.Roles
                                on userRole.RoleId
                                equals role.Id 
                                select role.Name).ToList()
                }).ToListAsync();
            return Ok(userList);
        }

        [Authorize(Policy = "RequiredAdminRole")]
        [HttpPost("editRoles/{username}")]
        public async Task<IActionResult> EditeRoles(string userName, RoleEditDto roleEditDto)
        {
            var user = await _userManager.FindByNameAsync(userName);
            
            var userRoles = await _userManager.GetRolesAsync(user);

            var selectedRoles = roleEditDto.RoleNames;

            selectedRoles = selectedRoles ?? new string[] {};

            var result = await _userManager.AddToRolesAsync(user, selectedRoles.Except(userRoles));

            if(!result.Succeeded)
                return BadRequest("failed to add to roles");
            result = await _userManager.RemoveFromRolesAsync(user, userRoles.Except(selectedRoles));
            if(!result.Succeeded)
                return BadRequest("Failed to remove roles");
             return Ok( await _userManager.GetRolesAsync(user));       
        }

        [Authorize(Policy = "ModeratorPhotoRole")]
        [HttpGet("photosForModerations")]
        public async Task<IActionResult> GetPhotoForModeration()
        {
            var photosPendingAprove = await  _repo.GetPhotosPendingAprove();                               
            return Ok(photosPendingAprove);
        }

        [Authorize(Policy ="ModeratorPhotoRole")]
        [HttpPost("approvePhoto/{photoId}")]
        public async Task<IActionResult> aprovePhotoUploaded(int photoId){
           var photoToAprove =  await _repo.GetPhoto(photoId);
           if(!(photoToAprove is null) && !photoToAprove.IsAproved){
                photoToAprove.IsAproved = true;
               // _repo.Add(photoToAprove);
                if(await _repo.SaveAll()){
                 return Ok();
                }
           }
           return BadRequest("Failed to aprove photo");
        }


        [Authorize(Policy = "ModeratorPhotoRole")]
        [HttpDelete("rejectPhoto/{photoId}")]
        public async Task<IActionResult> rejectPhoto(int photoId){
        var photoToDelete = await _repo.GetPhoto(photoId);
        if(!(photoToDelete is null))
        {
            if(photoToDelete.PublicId != null){
            var deleteParams = new DeletionParams(photoToDelete.PublicId);
            var result = _cloudinary.Destroy(deleteParams);
            if(result.Result !="ok"){
                return BadRequest("Failed to delete the photo");
            }
            }
            _repo.Delete(photoToDelete);
            
            if( await _repo.SaveAll())
             return Ok();

        }
        
        return BadRequest("Failed to reject photo");
        }

    }
}