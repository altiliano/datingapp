using System.Threading.Tasks;
using DatingApi.API.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DatingApi.API.Controllers
{
    [ApiController]
    [Route("Api/[Controller]")]
    public class ValuesController : ControllerBase
    {
        private readonly DataContext _dataContext;

        public ValuesController(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public DataContext DataContext { get; }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetValue(int id){
            var value = await _dataContext.Values.FirstOrDefaultAsync(x => x.Id == id);
            return Ok(value);          
        }

        [HttpGet]
        public async Task<IActionResult> GetValues(){
            var values = await _dataContext.Values.ToListAsync();
            return Ok(values);
        }
        
    }
}