using System.Linq;
using AutoMapper;
using DatingApi.API.Dtos;
using DatingApi.API.Models;

namespace DatingApi.API.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
           CreateMap<User, UserForListDto>()
            .ForMember(dest => dest.PhotoUrl, opt =>
                opt.MapFrom(src =>src.Photos.FirstOrDefault(p => p.IsMainPhoto).Url))
            .ForMember(dest => dest.Age, opt => opt.MapFrom(src => src.DateOfBirth.CalculateAge()));
           CreateMap<User, UserForDetailedDto>()
            .ForMember(dest => dest.PhotoUrl, opt =>
                opt.MapFrom(src =>src.Photos.FirstOrDefault(p => p.IsMainPhoto).Url))
            .ForMember(dest => dest.Age, opt => opt.MapFrom(src => src.DateOfBirth.CalculateAge()));;
           CreateMap<Photo, PhotoForDetailed>();

           CreateMap<UserforUpdateDto, User>(); 
           CreateMap<Photo,PhotoForReturnDto>();
           CreateMap<PhotoForCreationDto, Photo>();

           CreateMap<UserForRegisterDto,User>();
           CreateMap<MessageForCreationDto, Message>();
           CreateMap<Message, MessageForCreationDto>();
           CreateMap<Message,MessageToReturnDto>()
            .ForMember(m => m.SenderPhotoUrl, opt => opt.MapFrom( u => u.Sender.Photos.FirstOrDefault(p => p.IsMainPhoto).Url))
            .ForMember(m => m.RecipientPhotoUrl, opt => opt.MapFrom( u => u.Recipient.Photos.FirstOrDefault(p => p.IsMainPhoto).Url));

        }
    }
}